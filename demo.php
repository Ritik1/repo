<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>A Really Cool jQuery Gallery Demo | Tutorialzine</title>
	<link rel="stylesheet" type="text/css" href="lightbox/css/jquery.lightbox-0.5.css" />
	<link rel="stylesheet" type="text/css" href="demo.css" />
	<link href="https://fonts.googleapis.com/css?family=Allura|Mr+De+Haviland|Petit+Formal+Script" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
	<script type="text/javascript" src="lightbox/js/jquery.lightbox-0.5.pack.js"></script>
	<script type="text/javascript" src="script.js"></script>
</head>

<div id="container">
	<form action= "post.php" method= "POST"> 
	<p>Enter your name: <input type= "text" name= "name"> E-mail: <input type= "text" name= "email"></p> 
	<input type= "submit" value= "Log-in"> 
	<input type= "submit" value= "Register"> 
	</form>

	<div id="heading">
	<h1> You need just a few seconds...</h1>
	<h2>... to see how pretty is our world.</h2>
	</div>

<body>
<div id="gallery">

	<?php

		$directory = 'gallery';
		$allowed_types=array('jpg','jpeg','gif','png');
		$file_parts=array();
		$ext='';
		$title='';
		$i=0;

		$dir_handle = @opendir($directory) or die("There is an error with your image directory!");

		while ($file = readdir($dir_handle)) 
		{
			if($file=='.' || $file == '..') continue;
			
			$file_parts = explode('.',$file);
			$ext = strtolower(array_pop($file_parts));

			$title = implode('.',$file_parts);
			$title = htmlspecialchars($title);
			
			$nomargin='';
			
			if(in_array($ext,$allowed_types))
			{
				if(($i+1)%4==0) $nomargin='nomargin';
			
				echo '
				<div class="pic '.$nomargin.'" style="background:url('.$directory.'/'.$file.') no-repeat 50% 50%;">
				<a href="'.$directory.'/'.$file.'" title="'.$title.'" target="_blank">'.$title.'</a>
				</div>';
				
				$i++;
			}
		}

		closedir($dir_handle);

	?>
	<div class="clear"></div>
</div>

<div id="footer">
	<h2>Now, you see...?</h2>
</div>

</body>
</html>
